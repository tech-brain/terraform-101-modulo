provider "digitalocean" {}

resource "digitalocean_tag" "droplet-module" {
  name = "${var.tag}"
}

resource "digitalocean_droplet" "droplet-module" {
  image  = "${var.image}"
  name   = "${var.name}"
  region = "${var.region}"
  size   = "${var.size}"
  tags = ["${digitalocean_tag.droplet-module.id}"]
}